#Projet Cryptographie 
#Sujet 2

Dans ce Projet nous avons choisi la tailles 512 car c'est la plus petite et donc plus rapide à générer, ainsi que la 256 biensur

Le doublon_multi.c est la version multi-thread de doublon.c 
Pour ainsi privilégier un maximum de performance cette version est recommandé

openssl_github est l'openssl installé depuis Github dont on a modifié les rsa.h dans /crypto et /include afin de remplacer les tailles des clés pour pouvoir générer des clés 256 bits.

CREATEKEY
	. create_key.c est le fichier C qui permet la génération des clés RSA 
		si on souhaite modifier le taille des clés,le nombre de thread utilisés ainsi que le nombre de clés: il faut changer les define au début du code ligne 6-7-8
		
		on utilise le multi-thread dans ce programme.
		RSA *genKey(int taille) permet la génération de clés RSA
		
		int saveFile(RSA*rsaKey, int numero,int taille)  permet de sauvegarder les clés générés dans les fichiers .pem
		
		void *genSave(void *args)  permet d'effectuer le multi-threading correctement en utilisant mutex afin de lock et eviter les concurrences entre les threads et appeler les 2 autres fonctions cités précédemment
		
DOUBLON MULTI

		void cal_hachage(const RSA *rsa, unsigned char *hash) permet de calculer le hachage d'une clé mise en argument
		void *read_key(void *arg) permet de lire un fichier et calculer le hachage de sa clé
		int doublons() pour trouver les doublons entre les clés générées et utilise mutex
		
		
