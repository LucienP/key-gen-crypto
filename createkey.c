#include <stdio.h>
// Les librairies openssl pour pouvoir lire/ecrire les fichiers .pem et générer des clés
#include <openssl/rsa.h>
#include <openssl/pem.h> 
#include <pthread.h>

#define NUMEROkey 1000000 //nombre de clé
#define NUMEROthreads 1000 //nombre de thread
#define TAILLE 512 //taille de la clé

typedef struct
{
    int taille;
    int numero;
} 
ThreadArgs;

pthread_mutex_t lock = PTHREAD_MUTEX_INITIALIZER;
int a=0;

RSA *genKey(int taille) // générer les clés RSA
{
    RSA*rsaKey =RSA_new();
    BIGNUM *exponent=BN_new();
    BN_set_word(exponent, RSA_F4);
    // Génération de la paire de clés RSA
    RSA_generate_key_ex(rsaKey,taille, exponent,NULL);
    BN_free(exponent);
    return rsaKey;
}

int saveFile(RSA*rsaKey, int numero,int taille) 
{
    char pKey[100],puKey[100];
    // on genere les noms des fichiers
    sprintf(pKey,"privateKey%d.pem", numero);
    sprintf(puKey, "publicKey%d.pem",numero);
    FILE*pvKey=fopen(pKey,"wb");
    FILE*pubKey=fopen(puKey, "wb");
    PEM_write_RSAPrivateKey(pvKey,rsaKey, NULL, NULL, 0, NULL, NULL); // ecriture de la clé pv
    PEM_write_RSAPublicKey(pubKey,rsaKey); // ecriture de la clé public
    fclose(pvKey);
    fclose(pubKey);
    printf("Clé générée: 512 bits: %s, %s\n", pKey, puKey);
    return 0; // affichage à changer pour les clés de 256 bits
}

void *genSave(void *args) 
{
    while (1) // boucle infini
    {
        pthread_mutex_lock(&lock); // verrouillage mutex
        int keyGen=a++; // incrementation et stockage dans keyGen
        pthread_mutex_unlock(&lock); // deverrouillage
        if (keyGen >= NUMEROkey) // sortir quand toutes les clés sont générées
        {
        break;
        }
        RSA*rsaKey =genKey(TAILLE); // création d'une clé
        pthread_mutex_lock(&lock);
        saveFile(rsaKey, keyGen+1,TAILLE); // sauvegarde de la clé grâce à la fonction
        pthread_mutex_unlock(&lock);
        RSA_free(rsaKey);
    }
    pthread_exit(NULL); // thread terminé
}

int main() 
{
    pthread_t threads[NUMEROthreads];
    for (int i=0; i<NUMEROthreads;++i)
    {
    pthread_create(&threads[i],NULL,genSave,NULL); // création des threads
    }
    for (int i=0; i<NUMEROthreads;++i) // attente des threads
    {
    pthread_join(threads[i],NULL);
    }
    pthread_mutex_destroy(&lock); // destruction du mutex
    return 0;
}

