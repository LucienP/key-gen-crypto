#include <stdio.h>
#include <stdlib.h>
#include <openssl/rsa.h>
#include <openssl/pem.h>
//librairie pour accèder aux fonctions du calcul hachage comme SHA256()
#include <openssl/sha.h>
#include <string.h>

#define MAX_KEY_FILES 1000000 // nbr clés comparées
#define KEY_BITS 256 // taille clé
#define MAX_SIZE (KEY_BITS / 8) //taille octet

// Fonction pour calculer le hachage SHA256 d'une clé RSA
void cal_hachage(const RSA *rsa, unsigned char *hash) 
{
    unsigned char der[MAX_SIZE];
    //On va stocker temporairement la clé rsa qui est encoder en DER
    BIO *memoire = BIO_new(BIO_s_mem());
    PEM_write_bio_RSAPublicKey(memoire, rsa);
    int der_size = BIO_read(memoire, der, sizeof(der));
    // on utilise SHA256 pour calculer le hachage des données contenues, il y a en parametre le tableau der et la longueur des données dans der. Le résultat du hachage est stocké dans la variable hash.
    SHA256(der, der_size, hash);
    BIO_free(memoire);
}

// Fonction pour vérifier les doublons parmi les clés RSA
int doublons() 
{
    char File[MAX_KEY_FILES][50];
    RSA *rsa_key[MAX_KEY_FILES];
    unsigned char hach[MAX_KEY_FILES][SHA256_DIGEST_LENGTH];

    // Ouvrir les fichiers et calculer les hachages pour chaque clé
    for (int i = 0; i < MAX_KEY_FILES; ++i) 
    {
        sprintf(File[i], "privateKey%d.pem", i + 1);
        FILE *fichier = fopen(File[i], "rb");
        if (fichier == NULL) 
        {
            fprintf(stderr, "Erreur, fichier: %s impossible à ouvrir\n", File[i]);
            return 1;
        }
        rsa_key[i] = PEM_read_RSAPrivateKey(fichier, NULL, NULL, NULL);
        fclose(fichier);
        // Calculer le hachage SHA256 de la clé
        cal_hachage(rsa_key[i], hach[i]);
    }
    // Vérifier les doublons
    for (int i = 0; i < MAX_KEY_FILES - 1; ++i) 
    {
        for (int j = i + 1; j < MAX_KEY_FILES; ++j) 
        {
            if (memcmp(hach[i], hach[j], SHA256_DIGEST_LENGTH) == 0) 
            {
                printf("Il y a un doublon entre %s et %s\n", File[i], File[j]);
            }
        }
        printf("\n");
    }

    for (int x = 0; x < MAX_KEY_FILES; ++x) 
    {
        RSA_free(rsa_key[x]);
    }
    return 0;
}

int main() 
{
    if (doublons() == 0) 
        printf("Aucun doublon\n");
    else 
        return 1;
    return 0;
}

