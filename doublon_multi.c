#include <stdio.h>
#include <stdlib.h>
#include <openssl/rsa.h>
#include <openssl/pem.h>
//librairie pour accèder aux fonctions du calcul hachage comme SHA256()
#include <openssl/sha.h>
#include <string.h>
#include <pthread.h>

#define MAX_KEY_FILES 1000000 // nbr clés comparées
#define KEY_BITS 512 // taille clé
#define MAX_SIZE (KEY_BITS / 8) //taille octet

pthread_mutex_t lock; // Mutex pour la synchronisation

// Structure pour stocker les données d'une clé RSA et son hachage
typedef struct {
    RSA *rsa;
    unsigned char hash[SHA256_DIGEST_LENGTH];
} KeyData;

KeyData keys[MAX_KEY_FILES]; // Tableau pour stocker les clés et leurs hachages

// Fonction pour calculer le hachage SHA256 d'une clé RSA
void cal_hachage(const RSA *rsa, unsigned char *hash) 
{
    unsigned char der[MAX_SIZE];
    //On va stocker temporairement la clé rsa qui est encoder en DER
    BIO *memoire = BIO_new(BIO_s_mem());
    PEM_write_bio_RSAPublicKey(memoire, rsa);
    int der_size = BIO_read(memoire, der, sizeof(der));
    // on utilise SHA256 pour calculer le hachage des données contenues, il y a en parametre le tableau der et la longueur des données dans der. Le résultat du hachage est stocké dans la variable hash.
    SHA256(der, der_size, hash);
    BIO_free(memoire);
}

// Fonction pour lire un fichier et calculer le hachage de sa clé RSA
void *read_key(void *arg) 
{
    int index = *((int *)arg);
    char file_name[50];
    sprintf(file_name, "privateKey%d.pem", index + 1);
    FILE *file = fopen(file_name, "rb");
    //grace aux librairies on peut lire la clé .pem
    keys[index].rsa = PEM_read_RSAPrivateKey(file, NULL, NULL, NULL);
    fclose(file);
    cal_hachage(keys[index].rsa, keys[index].hash);
    pthread_exit(NULL);
}

// Fonction pour vérifier les doublons parmi les clés RSA
int doublons() 
{
    pthread_t threads[MAX_KEY_FILES];
    int indexes[MAX_KEY_FILES];
    pthread_mutex_init(&lock, NULL);
    for (int i = 0; i < MAX_KEY_FILES; ++i) {
        indexes[i] = i;
        if (pthread_create(&threads[i], NULL, read_key, &indexes[i]) != 0) {
            perror("Erreur lors de la création du thread");
            return 1;
        }
    }
    for (int i = 0; i < MAX_KEY_FILES; ++i) {
        pthread_join(threads[i], NULL);
    }
    pthread_mutex_destroy(&lock);

    int dup = 0;
    // comparer les hachages entre 2 fichiers
    for (int i = 0; i < MAX_KEY_FILES - 1; ++i) {
        for (int j = i + 1; j < MAX_KEY_FILES; ++j) {
	//compare les deux hachages octet par octet sur la longueur SHA256_DIGEST_LENGTH qui est la longueur du hachage 256 en octet
            if (memcmp(keys[i].hash, keys[j].hash, SHA256_DIGEST_LENGTH) == 0) {
                pthread_mutex_lock(&lock);
                printf("Doublon entre privateKey%d.pem et privateKey%d.pem\n", i + 1, j + 1);
                pthread_mutex_unlock(&lock);
                //on incremente le compteur
                dup++;
            }
        }
    }

    for (int i = 0; i < MAX_KEY_FILES; ++i) {
        RSA_free(keys[i].rsa);
    }
    return dup;
}

int main() 
{
    int dup = doublons();
    if (dup == 0) 
        printf("Aucun doublon \n");
    else {
        printf("Nombre total de doublons trouvés : %d\n", dup);
        return 1;
    }
    return 0;
}

