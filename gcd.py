import os
from cryptography.hazmat.primitives import serialization
from cryptography.hazmat.backends import default_backend
from cryptography.hazmat.primitives.asymmetric import rsa
from math import gcd



def lireKey(path, password=None): # lire la clé depuis un .pem
    try:
        with open(path,"rb") as key_file:  # ouvre le fichier
            pKey=serialization.load_pem_private_key ( # charge la clé
                key_file.read(),
                password=None,
                backend=default_backend()
            )
        return pKey
    except Exception as e:
        return None


def batchGCD(keys): # calcule le pgcd
    result=keys[0].public_key().public_numbers().n # résultat prends le modulo de la première clé
    for key in keys[1:]: # calcule le pgcd à partir de la 2eme clé
        result=gcd(result,key.public_key().public_numbers().n)
    return result



keyD="/home/vincent/key" # vers le dossier key contenant les clés
test=os.listdir(keyD) # on accède à la liste des fichiers
pvKey=[file for file in test if file.startswith("privateKey")] # on garde les fichiers
a=[os.path.join(keyD, file) for file in pvKey] # rassemblage du dossier et du fichier
password=None
rsa=[lireKey(path, password) for path in a]
rsa=[key for key in rsa if key is not None] # filtre des clés illisible
result_gcd=batchGCD(rsa) # calcule du pgcd
print(f"Le pgcd est de: {result_gcd}")












